"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RandomBackgroundColorDirective = void 0;
var core_1 = require("@angular/core");
var RandomBackgroundColorDirective = /** @class */ (function () {
    function RandomBackgroundColorDirective(el) {
        /**
         * Some selected background color hex codes
         *
         */
        this.backgroundColors = ["#1D8348", "#E74C3C", "#9B59B6", "#8E44AD", "#2980B9", "#3498DB", "#1ABC9C", "#16A085", "#27AE60", "#2ECC71",
            "F1C40F", "#F39C12", "#E67E22", "#D35400", "#ECF0F1", "#BDC3C7", "#95A5A6", "#7F8C8D", "#34495E", "#2C3E50"];
        el.nativeElement.style.backgroundColor = this.getRandomBackground();
    }
    /**
     * using Math.random() a random method will be generated,
     * and the item at that index will be returned as backgrond color
     */
    RandomBackgroundColorDirective.prototype.getRandomBackground = function () {
        return this.backgroundColors[Math.floor(Math.random() * this.backgroundColors.length)];
    };
    RandomBackgroundColorDirective = __decorate([
        core_1.Directive({
            selector: '[appRandomBackgroundColor]',
            host: {}
        })
        /**
         * this directive is created for changing the background color elemnt on which
         * directive is used
         * it  takes el:ElementRef as a argument in constructor
         * when the constructor is initiated , the method getRandomBackground() will be called
         * and returned background color will be assigned to that elemnt as a background color
         *
         */
    ], RandomBackgroundColorDirective);
    return RandomBackgroundColorDirective;
}());
exports.RandomBackgroundColorDirective = RandomBackgroundColorDirective;
