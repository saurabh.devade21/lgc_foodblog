"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ImageerrorDirective = void 0;
var core_1 = require("@angular/core");
/**
 * this custom directive is created for changing the url
 * of broken images
 * in the host (error) is mentioned
 * this event will be triggred once the image tag will found the broken image,
 * once this event is triggred the updatedUrl() function will be called
 * this function will replace the broken image url with the appImageerror
 *
 * @param appImageerror is also passed from the html element
 * eg. appImageerror="{{imagesBaseUrl+'no-image-small.jpg'}}
 * this how directive is used
 *
 */
var ImageerrorDirective = /** @class */ (function () {
    /**
     *@appImageerror the url to be updaed passed from the directive
     */
    function ImageerrorDirective() {
    }
    ImageerrorDirective.prototype.updateUrl = function () {
        this.src = this.appImageerror;
    };
    __decorate([
        core_1.Input()
    ], ImageerrorDirective.prototype, "src");
    __decorate([
        core_1.Input()
    ], ImageerrorDirective.prototype, "appImageerror");
    __decorate([
        core_1.HostBinding('class')
    ], ImageerrorDirective.prototype, "className");
    ImageerrorDirective = __decorate([
        core_1.Directive({
            selector: '[appImageerror]',
            host: {
                '(error)': 'updateUrl()',
                '[src]': 'src'
            }
        })
    ], ImageerrorDirective);
    return ImageerrorDirective;
}());
exports.ImageerrorDirective = ImageerrorDirective;
