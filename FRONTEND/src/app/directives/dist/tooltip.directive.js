"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TooltipDirective = void 0;
var core_1 = require("@angular/core");
/**
 * this directive is used for copying the data to the clipboard and showing a tooltip
 * that the particular text is copied
 * in host (click) event is registered
 * once click is regitred it will call the function showTooltip()
 */
var TooltipDirective = /** @class */ (function () {
    function TooltipDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    /**
     * this function executes in the followinf steps
     * 1. ceated a text area
     * 2. set styling in a such way that it wont be visible to user on the page
     * 3. sets the value of textbox to the browser url
     * 4. focus, select and execCommand('copy') to get the textbox value to the clipboard
     * 5. Removes the created textbox
     * 6. shows tooltip by appending a span element as a child
     * 7. after 500 ms removes that child
     */
    TooltipDirective.prototype.showTooltip = function () {
        var selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = window.location.href;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
        var span = this.renderer.createElement('span');
        var text = this.renderer.createText(this.appTooltip);
        this.renderer.addClass(span, 'clipboard');
        this.renderer.appendChild(span, text);
        this.renderer.appendChild(this.el.nativeElement, span);
        var var1 = this.renderer;
        var var2 = this.el;
        setTimeout(function () { var1.removeChild(var2, span); }, 500);
    };
    __decorate([
        core_1.Input()
    ], TooltipDirective.prototype, "appTooltip");
    TooltipDirective = __decorate([
        core_1.Directive({
            selector: '[appTooltip]',
            host: {
                '(click)': 'showTooltip()'
            }
        })
    ], TooltipDirective);
    return TooltipDirective;
}());
exports.TooltipDirective = TooltipDirective;
