"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ScrolltotargetDirective = void 0;
var core_1 = require("@angular/core");
/**
 * Directive for scrolling to the target passed
 * in host (click) event is registred
 * once this event is triggred from the elemnt it wil call the scrollToTarget() function
 * @param input is the target on to which it will scroll
 *
 */
var ScrolltotargetDirective = /** @class */ (function () {
    function ScrolltotargetDirective() {
    }
    /**
     * to scroll to the element specified in the target
     * with smooth behavior
     * it also has setTimeout function, reason to use set timeout is it waits for the div to become appear if that is hidden already
     *
     */
    ScrolltotargetDirective.prototype.scrollToTarget = function () {
        var _this = this;
        setTimeout(function () {
            document.querySelector('#' + _this.appScrolltotarget).scrollIntoView({ behavior: 'smooth' });
        }, 300);
    };
    __decorate([
        core_1.Input()
    ], ScrolltotargetDirective.prototype, "appScrolltotarget");
    ScrolltotargetDirective = __decorate([
        core_1.Directive({
            selector: '[appScrolltotarget]',
            host: {
                '(click)': 'scrollToTarget()'
            }
        })
    ], ScrolltotargetDirective);
    return ScrolltotargetDirective;
}());
exports.ScrolltotargetDirective = ScrolltotargetDirective;
