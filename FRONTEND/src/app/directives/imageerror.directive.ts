import {Directive, Input, HostBinding} from '@angular/core'


/**
 * this custom directive is created for changing the url
 * of broken images
 * in the host (error) is mentioned
 * this event will be triggred once the image tag will found the broken image,
 * once this event is triggred the updatedUrl() function will be called
 * this function will replace the broken image url with the appImageerror
 *
 * @param appImageerror is also passed from the html element
 * eg. appImageerror="{{imagesBaseUrl+'no-image-small.jpg'}}
 * this how directive is used
 *
 */
@Directive({
  selector: '[appImageerror]',
  host: {
    '(error)':'updateUrl()',
    '[src]':'src'
   }
})

export class ImageerrorDirective {

  /**
   *@appImageerror the url to be updaed passed from the directive
   */
  constructor() { }
  @Input() src:string;
  @Input() appImageerror:string;
  @HostBinding('class') className

  updateUrl() {
    this.src = this.appImageerror;
  }

}
