import { Directive, Input } from '@angular/core';


/**
 * Directive for scrolling to the target passed
 * in host (click) event is registred
 * once this event is triggred from the elemnt it wil call the scrollToTarget() function
 * @param input is the target on to which it will scroll
 *
 */
@Directive({
  selector: '[appScrolltotarget]',
  host: {
    '(click)': 'scrollToTarget()',
  }
})
export class ScrolltotargetDirective {

  constructor() { }
  @Input() appScrolltotarget: string;

  /**
   * to scroll to the element specified in the target
   * with smooth behavior
   * it also has setTimeout function, reason to use set timeout is it waits for the div to become appear if that is hidden already
   *
   */
  scrollToTarget() {
    setTimeout(() => {
      document.querySelector('#' + this.appScrolltotarget).scrollIntoView({ behavior: 'smooth' });
    }, 300);
  }
}
