import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

/**
 * this directive is used for copying the data to the clipboard and showing a tooltip
 * that the particular text is copied
 * in host (click) event is registered
 * once click is regitred it will call the function showTooltip()
 */
@Directive({
  selector: '[appTooltip]',
  host: {
    '(click)': 'showTooltip()',
  }
})
export class TooltipDirective {
  @Input() appTooltip: string;

  constructor(private el: ElementRef, private renderer: Renderer2) { }


  /**
   * this function executes in the followinf steps
   * 1. ceated a text area
   * 2. set styling in a such way that it wont be visible to user on the page
   * 3. sets the value of textbox to the browser url
   * 4. focus, select and execCommand('copy') to get the textbox value to the clipboard
   * 5. Removes the created textbox
   * 6. shows tooltip by appending a span element as a child
   * 7. after 500 ms removes that child
   */
  showTooltip() {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = window.location.href;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    const span = this.renderer.createElement('span');
    const text = this.renderer.createText(this.appTooltip);
    this.renderer.addClass(span, 'clipboard');

    this.renderer.appendChild(span, text);
    this.renderer.appendChild(this.el.nativeElement, span);
    const var1 = this.renderer;
    const var2 = this.el;
    setTimeout(function () { var1.removeChild(var2, span); }, 500);
  }

}
