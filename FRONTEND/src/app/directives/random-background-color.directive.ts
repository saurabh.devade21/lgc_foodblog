import { Directive, ElementRef, HostBinding } from '@angular/core';

@Directive({
  selector: '[appRandomBackgroundColor]',
  host: {  }
})
/**
 * this directive is created for changing the background color elemnt on which
 * directive is used
 * it  takes el:ElementRef as a argument in constructor
 * when the constructor is initiated , the method getRandomBackground() will be called
 * and returned background color will be assigned to that elemnt as a background color
 *
 */
export class RandomBackgroundColorDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = this.getRandomBackground();
 }

/**
 * Some selected background color hex codes
 *
 */
  backgroundColors=["#1D8348","#E74C3C","#9B59B6","#8E44AD","#2980B9","#3498DB","#1ABC9C","#16A085","#27AE60","#2ECC71",
  "F1C40F","#F39C12","#E67E22","#D35400","#ECF0F1","#BDC3C7","#95A5A6","#7F8C8D","#34495E","#2C3E50"];

 /**
  * using Math.random() a random method will be generated,
  * and the item at that index will be returned as backgrond color
  */
  getRandomBackground(){
    return this.backgroundColors[Math.floor(Math.random() * this.backgroundColors.length)];
  }



}
