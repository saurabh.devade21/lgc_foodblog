// animations.ts
import { trigger, state, style, transition, animate } from '@angular/animations';



/***
 *This is custom animation constant written seperately to get the effect of hide and show
 *this animation constant is to be imported in como=ponent to be used in animation array
 *the hide state is basically keeps the hight of element 0 and makes overflow hidded
 *once it goes to show state it makes element hight as needed
 *then transition functions animates the element based on time provided in miliseconds
 */
export const Animations = {
  hideshowanimation: trigger('hideshowanimation', [
      state('hide', style({
        height:'0px',
        overflow:'hidden',
        // opacity:0
      })),
      state('show', style({
        height: '*',
        // opacity:1
      })),
      transition('hide=>show', animate('500ms')),
      transition('show=>hide', animate('500ms'))
    ]),

}
