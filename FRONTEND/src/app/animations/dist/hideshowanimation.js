"use strict";
exports.__esModule = true;
exports.HideshowAnimation = void 0;
// animations.ts
var animations_1 = require("@angular/animations");
exports.HideshowAnimation = {
    hideshowanimation: animations_1.trigger('hideshowanimation', [
        animations_1.state('hide', animations_1.style({
            height: '0px',
            overflow: 'hidden'
        })),
        animations_1.state('show', animations_1.style({
            height: '*'
        })),
        animations_1.transition('hide=>show', animations_1.animate('500ms')),
        animations_1.transition('show=>hide', animations_1.animate('500ms'))
    ])
};
