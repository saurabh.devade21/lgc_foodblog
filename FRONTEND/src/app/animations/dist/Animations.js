"use strict";
exports.__esModule = true;
exports.Animations = void 0;
// animations.ts
var animations_1 = require("@angular/animations");
/***
 *This is custom animation constant written seperately to get the effect of hide and show
 *this animation constant is to be imported in como=ponent to be used in animation array
 *the hide state is basically keeps the hight of element 0 and makes overflow hidded
 *once it goes to show state it makes element hight as needed
 *then transition functions animates the element based on time provided in miliseconds
 */
exports.Animations = {
    hideshowanimation: animations_1.trigger('hideshowanimation', [
        animations_1.state('hide', animations_1.style({
            height: '0px',
            overflow: 'hidden'
        })),
        animations_1.state('show', animations_1.style({
            height: '*'
        })),
        animations_1.transition('hide=>show', animations_1.animate('500ms')),
        animations_1.transition('show=>hide', animations_1.animate('500ms'))
    ])
};
