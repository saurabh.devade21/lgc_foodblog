"use strict";
exports.__esModule = true;
exports.Blogpost = void 0;
var Blogpost = /** @class */ (function () {
    function Blogpost() {
    }
    Object.defineProperty(Blogpost.prototype, "getMessage", {
        get: function () { return this.id; },
        enumerable: false,
        configurable: true
    });
    ;
    return Blogpost;
}());
exports.Blogpost = Blogpost;
