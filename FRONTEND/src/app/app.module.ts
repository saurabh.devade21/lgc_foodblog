import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpdataService } from './services/httpdata.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FeedComponent } from './components/feed/feed.component';
import { HttpClientModule } from '@angular/common/http';
import { BlogdetailsComponent } from './components/blogdetails/blogdetails.component';
import { FirstcharonlyPipe } from './pipes/firstcharonly.pipe';
import { FormsModule } from '@angular/forms';
import { AddblogpostComponent } from './components/addblogpost/addblogpost.component';
import { ImageerrorDirective } from './directives/imageerror.directive';
import { FilterpostPipe } from './pipes/filterpost.pipe';
import { ScrolltotargetDirective } from './directives/scrolltotarget.directive';
import { RandomBackgroundColorDirective } from './directives/random-background-color.directive';
import { TooltipDirective } from './directives/tooltip.directive';
import { LoaderComponent } from './components/loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FeedComponent,
    BlogdetailsComponent,
    FirstcharonlyPipe,
    AddblogpostComponent,
    ImageerrorDirective,
    FilterpostPipe,
    ScrolltotargetDirective,
    RandomBackgroundColorDirective,
    TooltipDirective,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [HttpdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
