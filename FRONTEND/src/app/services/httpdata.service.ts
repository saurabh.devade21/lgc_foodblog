import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

/**
 * This service is used across the entire application for for fetching the data from the server
 * using rest APIs
 *
 */

@Injectable({
  providedIn: 'root'
})
export class HttpdataService {

  /**
   * Base urls of developmnet and live servers
   *
   */
   private BASE_URL = "http://localhost:3000/";
  //private BASE_URL = "https://lgcfoodblogtest.herokuapp.com/";

  constructor(private httpClient: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //window.alert(errorMessage);
    return throwError(errorMessage);
  }


  //Getting all the blogposts from the server
  public getAllBlogPosts() {
    return this.httpClient.get(this.BASE_URL + "posts").pipe(catchError(this.handleError));
  }
  //Getting the details of particular blog posts
  public getBlogPostDetailsById(postId) {
    return this.httpClient.get(this.BASE_URL + "posts/" + postId).pipe(catchError(this.handleError));
  }

  //Getting all comments of the post
  public getBlogPostCommentsById(postId) {
    return this.httpClient.get(this.BASE_URL + "posts/" + postId + "/comments").pipe(catchError(this.handleError));
  }
  //posting new comment for the blog post
  public postComment(postData, postId) {
    return this.httpClient.post(this.BASE_URL + "posts/" + postId + "/comments", JSON.stringify(postData), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
  //posting the new blog
  public postBlog(postData) {
    return this.httpClient.post(this.BASE_URL + "posts/", JSON.stringify(postData), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }
}
