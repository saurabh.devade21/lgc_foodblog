"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = void 0 && (void 0).__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

exports.__esModule = true;
exports.HttpdataService = void 0;

var core_1 = require("@angular/core");

var http_1 = require("@angular/common/http");

var rxjs_1 = require("rxjs");

var operators_1 = require("rxjs/operators");

var HttpdataService =
/** @class */
function () {
  function HttpdataService(httpClient) {
    this.httpClient = httpClient;
    this.BASE_URL = "http://localhost:8080/";
    this.httpOptions = {
      headers: new http_1.HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  HttpdataService.prototype.handleError = function (error) {
    var errorMessage = 'Unknown error!';

    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = "Error: " + error.error.message;
    } else {
      // Server-side errors
      errorMessage = "Error Code: " + error.status + "\nMessage: " + error.message;
    }

    window.alert(errorMessage);
    return rxjs_1.throwError(errorMessage);
  };

  HttpdataService.prototype.getAllBlogPosts = function () {
    return this.httpClient.get(this.BASE_URL + "posts").pipe(operators_1.catchError(this.handleError));
  };

  HttpdataService.prototype.getBlogPostDetailsById = function (postId) {
    return this.httpClient.get(this.BASE_URL + "posts/" + postId).pipe(operators_1.catchError(this.handleError));
  };

  HttpdataService.prototype.getBlogPostCommentsById = function (postId) {
    return this.httpClient.get(this.BASE_URL + "posts/" + postId + "/comments").pipe(operators_1.catchError(this.handleError));
  };

  HttpdataService.prototype.postComment = function (postData, postId) {
    return this.httpClient.post(this.BASE_URL + "posts/" + postId + "/comments", JSON.stringify(postData), this.httpOptions).pipe(operators_1.catchError(this.handleError));
  };

  HttpdataService.prototype.postBlog = function (postData) {
    return this.httpClient.post(this.BASE_URL + "posts/", JSON.stringify(postData), this.httpOptions).pipe(operators_1.catchError(this.handleError));
  };

  HttpdataService = __decorate([core_1.Injectable({
    providedIn: 'root'
  })], HttpdataService);
  return HttpdataService;
}();

exports.HttpdataService = HttpdataService;