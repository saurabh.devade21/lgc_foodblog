"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HttpdataService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
/**
 * This service is used across the entire application for for fetching the data from the server
 * using rest APIs
 *
 */
var HttpdataService = /** @class */ (function () {
    //private BASE_URL = "https://lgcfoodblogtest.herokuapp.com/";
    function HttpdataService(httpClient) {
        this.httpClient = httpClient;
        /**
         * Base urls of developmnet and live servers
         *
         */
        this.BASE_URL = "http://localhost:3000/";
        this.httpOptions = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    HttpdataService.prototype.handleError = function (error) {
        var errorMessage = 'Unknown error!';
        if (error.error instanceof ErrorEvent) {
            // Client-side errors
            errorMessage = "Error: " + error.error.message;
        }
        else {
            // Server-side errors
            errorMessage = "Error Code: " + error.status + "\nMessage: " + error.message;
        }
        //window.alert(errorMessage);
        return rxjs_1.throwError(errorMessage);
    };
    //Getting all the blogposts from the server
    HttpdataService.prototype.getAllBlogPosts = function () {
        return this.httpClient.get(this.BASE_URL + "posts").pipe(operators_1.catchError(this.handleError));
    };
    //Getting the details of particular blog posts
    HttpdataService.prototype.getBlogPostDetailsById = function (postId) {
        return this.httpClient.get(this.BASE_URL + "posts/" + postId).pipe(operators_1.catchError(this.handleError));
    };
    //Getting all comments of the post
    HttpdataService.prototype.getBlogPostCommentsById = function (postId) {
        return this.httpClient.get(this.BASE_URL + "posts/" + postId + "/comments").pipe(operators_1.catchError(this.handleError));
    };
    //posting new comment for the blog post
    HttpdataService.prototype.postComment = function (postData, postId) {
        return this.httpClient.post(this.BASE_URL + "posts/" + postId + "/comments", JSON.stringify(postData), this.httpOptions)
            .pipe(operators_1.catchError(this.handleError));
    };
    //posting the new blog
    HttpdataService.prototype.postBlog = function (postData) {
        return this.httpClient.post(this.BASE_URL + "posts/", JSON.stringify(postData), this.httpOptions)
            .pipe(operators_1.catchError(this.handleError));
    };
    HttpdataService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], HttpdataService);
    return HttpdataService;
}());
exports.HttpdataService = HttpdataService;
