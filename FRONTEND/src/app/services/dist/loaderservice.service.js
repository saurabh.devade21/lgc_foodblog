"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoaderserviceService = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
/**
 * This service is responsib le for hiding and showing the loading screen all across the page
 *
 *
 */
var LoaderserviceService = /** @class */ (function () {
    /**
     * it uses .subscribe() to keep track fo the isLoaderVisible flag
     */
    function LoaderserviceService() {
        var _this = this;
        this.isLoaderVisible = false;
        this.loaderVisibilityChange = new rxjs_1.Subject();
        this.loaderVisibilityChange.subscribe(function (value) {
            _this.isLoaderVisible = value;
        });
    }
    /**
     * to toggle the loader visiblity
     */
    LoaderserviceService.prototype.toggleSidebarVisibility = function () {
        this.loaderVisibilityChange.next(!this.isLoaderVisible);
        return this.isLoaderVisible;
    };
    /**
     * to show the loader on the page
     */
    LoaderserviceService.prototype.showLoader = function () {
        this.isLoaderVisible = true;
        return this.isLoaderVisible;
    };
    /**
     * to hide the loader
     */
    LoaderserviceService.prototype.hideLoader = function () {
        this.isLoaderVisible = false;
        return this.isLoaderVisible;
    };
    LoaderserviceService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], LoaderserviceService);
    return LoaderserviceService;
}());
exports.LoaderserviceService = LoaderserviceService;
