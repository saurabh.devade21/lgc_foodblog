"use strict";
exports.__esModule = true;
var http_1 = require("@angular/common/http");
var testing_1 = require("@angular/core/testing");
var httpdata_service_1 = require("./httpdata.service");
describe('HttpdataService', function () {
    beforeEach(function () { return testing_1.TestBed.configureTestingModule({
        imports: [
            http_1.HttpClientModule
        ],
        declarations: []
    }); });
    it('should be created', function () {
        var service = testing_1.TestBed.get(httpdata_service_1.HttpdataService);
        expect(service).toBeTruthy();
    });
});
