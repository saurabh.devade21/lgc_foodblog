import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { HttpdataService } from './httpdata.service';

describe('HttpdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    declarations: [

    ],
  }));

  it('should be created', () => {
    const service: HttpdataService = TestBed.get(HttpdataService);
    expect(service).toBeTruthy();
  });
});
