import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


/**
 * This service is responsib le for hiding and showing the loading screen all across the page
 *
 *
 */
@Injectable({
  providedIn: 'root'
})
export class LoaderserviceService {

  isLoaderVisible: boolean=false;

    loaderVisibilityChange: Subject<boolean> = new Subject<boolean>();

    /**
     * it uses .subscribe() to keep track fo the isLoaderVisible flag
     */
    constructor()  {
        this.loaderVisibilityChange.subscribe((value) => {
            this.isLoaderVisible = value
        });
    }

    /**
     * to toggle the loader visiblity
     */
    toggleSidebarVisibility() {
        this.loaderVisibilityChange.next(!this.isLoaderVisible);
        return this.isLoaderVisible;
    }
    /**
     * to show the loader on the page
     */
    showLoader(){
      this.isLoaderVisible=true;
      return this.isLoaderVisible;
    }
    /**
     * to hide the loader
     */
    hideLoader(){
      this.isLoaderVisible=false;
      return this.isLoaderVisible;
    }

}
