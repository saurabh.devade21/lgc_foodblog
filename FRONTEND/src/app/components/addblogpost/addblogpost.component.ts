import { LoaderserviceService } from './../../services/loaderservice.service';
import { HttpdataService } from './../../services/httpdata.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-addblogpost',
  templateUrl: './addblogpost.component.html',
  styleUrls: ['./addblogpost.component.scss']
})
export class AddblogpostComponent implements OnInit {
  model: any = { poster: "" };
  @Input() myMethod: Function;
  loaderStatus: boolean;

  constructor(
    private httpdService: HttpdataService,
    private loaderserviceService: LoaderserviceService) { }

  //Eventemitter for getting all blogposts in feedComponent
  @Output("feedComponentGetAllPost") feedComponentGetAllPost: EventEmitter<any> = new EventEmitter();

  //Evetemitter for changing the form visibility status
  @Output("feedComponentChangeState") feedComponentChangeState: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

  /**
   *
   * @param form form reference to get access to the input field
   * this is function is written for checking whether image for entered url is exists and valid
   * this method gets invoked when default (error) method on image tag is called
   * once this method is called the  'poster' form.controls['poster'].setErrors({ 'incorrect': true });
   *  form field is set to incorrect so that error message in the form will be displayed
   *  it will also check for if the entered url is empty, if the url is empty the form field is again set to valid
   * so user can proceed with the form submition without image.
   *
   */
  imageError(form) {
    form.controls['poster'].setErrors({ 'incorrect': true });
    if (form.controls['poster'].value === "") {
      form.controls['poster'].setErrors(null);
    }
  }

  /****
   * this method is responsible for taking the values from input fields
   * and posting the data to the server
   *
   * @param form it takes reference of form as the parameter
   * the loaderserviceService.showLoader() & loaderserviceService.hideLoader()
   * function is to make loader hide and show
   *
   * then postdata object creates a post data to be sent to the server
   * then it calls the method from httpservice postBlog(postdata) and subscribes to that.
   * once the request is successfull it emmits the parent component events, which are catched in
   * feedcomponent and the it again gets the all posts and change the status of form visibility, so that
   * recent posted feed will also be shown immediately
   *
   * it also catches any error occured
   *
   *
   *
   */
  postBlog(form) {

    this.loaderStatus = this.loaderserviceService.showLoader();
    const postdata = {
      "title": this.model.title,
      "author": this.model.author,
      "publish_date": new Date(),
      "description": this.model.description,
      "content": this.model.content,
      "poster": this.model.poster
    }
    this.httpdService.postBlog(postdata).subscribe((data: any[]) => {




      this.httpdService.getAllBlogPosts().subscribe(

        (data: any[]) => {
          this.feedComponentGetAllPost.emit();
          this.feedComponentChangeState.emit();
          form.reset();
        },
        (catchError) => {
          console.log('HTTP Error', catchError);
          this.loaderStatus = this.loaderserviceService.hideLoader();
        },
        () => {
          this.loaderStatus = this.loaderserviceService.hideLoader();
        })
    })
  }

}
