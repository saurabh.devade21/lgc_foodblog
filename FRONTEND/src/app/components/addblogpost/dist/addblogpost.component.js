"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AddblogpostComponent = void 0;
var core_1 = require("@angular/core");
var AddblogpostComponent = /** @class */ (function () {
    function AddblogpostComponent(httpdService, loaderserviceService) {
        this.httpdService = httpdService;
        this.loaderserviceService = loaderserviceService;
        this.model = { poster: "" };
        //Eventemitter for getting all blogposts in feedComponent
        this.feedComponentGetAllPost = new core_1.EventEmitter();
        //Evetemitter for changing the form visibility status
        this.feedComponentChangeState = new core_1.EventEmitter();
    }
    AddblogpostComponent.prototype.ngOnInit = function () {
    };
    /**
     *
     * @param form form reference to get access to the input field
     * this is function is written for checking whether image for entered url is exists and valid
     * this method gets invoked when default (error) method on image tag is called
     * once this method is called the  'poster' form.controls['poster'].setErrors({ 'incorrect': true });
     *  form field is set to incorrect so that error message in the form will be displayed
     *  it will also check for if the entered url is empty, if the url is empty the form field is again set to valid
     * so user can proceed with the form submition without image.
     *
     */
    AddblogpostComponent.prototype.imageError = function (form) {
        form.controls['poster'].setErrors({ 'incorrect': true });
        if (form.controls['poster'].value === "") {
            form.controls['poster'].setErrors(null);
        }
    };
    /****
     * this method is responsible for taking the values from input fields
     * and posting the data to the server
     *
     * @param form it takes reference of form as the parameter
     * the loaderserviceService.showLoader() & loaderserviceService.hideLoader()
     * function is to make loader hide and show
     *
     * then postdata object creates a post data to be sent to the server
     * then it calls the method from httpservice postBlog(postdata) and subscribes to that.
     * once the request is successfull it emmits the parent component events, which are catched in
     * feedcomponent and the it again gets the all posts and change the status of form visibility, so that
     * recent posted feed will also be shown immediately
     *
     * it also catches any error occured
     *
     *
     *
     */
    AddblogpostComponent.prototype.postBlog = function (form) {
        var _this = this;
        this.loaderStatus = this.loaderserviceService.showLoader();
        var postdata = {
            "title": this.model.title,
            "author": this.model.author,
            "publish_date": new Date(),
            "description": this.model.description,
            "content": this.model.content,
            "poster": this.model.poster
        };
        this.httpdService.postBlog(postdata).subscribe(function (data) {
            _this.httpdService.getAllBlogPosts().subscribe(function (data) {
                _this.feedComponentGetAllPost.emit();
                _this.feedComponentChangeState.emit();
                form.reset();
            }, function (catchError) {
                console.log('HTTP Error', catchError);
                _this.loaderStatus = _this.loaderserviceService.hideLoader();
            }, function () {
                _this.loaderStatus = _this.loaderserviceService.hideLoader();
            });
        });
    };
    __decorate([
        core_1.Input()
    ], AddblogpostComponent.prototype, "myMethod");
    __decorate([
        core_1.Output("feedComponentGetAllPost")
    ], AddblogpostComponent.prototype, "feedComponentGetAllPost");
    __decorate([
        core_1.Output("feedComponentChangeState")
    ], AddblogpostComponent.prototype, "feedComponentChangeState");
    AddblogpostComponent = __decorate([
        core_1.Component({
            selector: 'app-addblogpost',
            templateUrl: './addblogpost.component.html',
            styleUrls: ['./addblogpost.component.scss']
        })
    ], AddblogpostComponent);
    return AddblogpostComponent;
}());
exports.AddblogpostComponent = AddblogpostComponent;
