import { Animations } from './../../animations/Animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { FirstcharonlyPipe } from './../../pipes/firstcharonly.pipe';
import { FormsModule } from '@angular/forms';
import { ImageerrorDirective } from './../../directives/imageerror.directive';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogdetailsComponent } from './blogdetails.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component, Directive, Type } from '@angular/core';

export function MockDirective(options: Component): Type<Directive> {
  const metadata: Directive = {
    selector: options.selector,
    inputs: options.inputs,
    outputs: options.outputs,
  };
  return Directive(metadata)(class MockDirectiveClass {
  });
}
describe('BlogdetailsComponent', () => {
  let component: BlogdetailsComponent;
  let fixture: ComponentFixture<BlogdetailsComponent>;


   @Component({selector: 'app-loader', template: ''})
   class LoaderComponent {}



  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [BlogdetailsComponent, LoaderComponent, ImageerrorDirective, FirstcharonlyPipe,MockDirective({
        selector: '[appImageerror]',
        inputs: []  // <--- empty, unless the directive has inputs
      })],
      imports: [FormsModule],

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
