"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BlogdetailsComponent = void 0;
var Animations_1 = require("./../../animations/Animations");
var core_1 = require("@angular/core");
/**
 *This component shows details and comments of the blog
 *it takes the blog id from url
 */
var BlogdetailsComponent = /** @class */ (function () {
    function BlogdetailsComponent(route, httpdService, router, serverendpoints, loaderserviceService) {
        this.route = route;
        this.httpdService = httpdService;
        this.router = router;
        this.serverendpoints = serverendpoints;
        this.loaderserviceService = loaderserviceService;
        this.postDetails = {};
        this.postComments = [];
        this.model = {};
        this.iconsBaseUrl = this.serverendpoints.iconsBaseUrl;
        this.imagesBaseUrl = this.serverendpoints.imageBaseUrl;
        this.likeButtonImage = this.iconsBaseUrl + "heart.png";
        /**
         * This functinality is implemented to assign the hide show animation on clicking the animation
         */
        this.currentState = 'hide';
    }
    BlogdetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.blogId = +params.get('id');
            _this.getBlogPostDetails(_this.blogId);
        });
        //this.iconsBaseUrl=this.serverendpoints.iconsBaseUrl;
    };
    /**
     * this function is repsonsible for getting the blog details of the blog id from the browser url
     * @param blogId id of the blog scraped from url
     * this.loaderserviceService.showLoader() & this.loaderserviceService.hideLoader() to hide and show the loader
     * then it calles getBlogPostDetailsById() method from the httpdservice and subscribe to that
     *
     * if th blog details are recieved succesully it will call the api for getting all the comments of that posts
     * it will store the recieved data in this.postdetails variable which will be shown on the page
     */
    BlogdetailsComponent.prototype.getBlogPostDetails = function (blogId) {
        var _this = this;
        this.loaderStatus = this.loaderserviceService.showLoader();
        this.httpdService.getBlogPostDetailsById(blogId).subscribe(function (data) {
            _this.postDetails = data;
            _this.getBlogPostComments(blogId);
        }, function (catchError) {
            console.log('HTTP Error', catchError);
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        }, function () {
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        });
    };
    /**
     * the methods retrive all the comments posted for the blog
     * @param blogId id of the blog scraped from url
     * once the api is success it recieves the data and  using array.sort and arrow function sorts the array
     * by data and assign to this.postComments
     */
    BlogdetailsComponent.prototype.getBlogPostComments = function (blogId) {
        var _this = this;
        this.loaderStatus = this.loaderserviceService.showLoader();
        this.httpdService.getBlogPostCommentsById(blogId).subscribe(function (data) {
            _this.postComments = data.sort(function (a, b) { return new Date(b.date).getTime() - new Date(a.date).getTime(); });
        }, function (catchError) {
            console.log('HTTP Error', catchError);
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        }, function () {
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        });
    };
    BlogdetailsComponent.prototype.changeState = function () {
        this.currentState = this.currentState === 'hide' ? 'show' : 'hide';
    };
    /**
     *
     * @param form reference of form
     *
     * this method is used for posting the comment for a blog
     * this.loaderserviceService.showLoader() & this.loaderserviceService.hideLoader() to hide and show the loader
     * once the api is succesfully called it gets the all comments again from function getBlogPostComments()
     * and hides the form using changeState
     * finally it resets the form by clearing input fields
     *
     */
    BlogdetailsComponent.prototype.postComment = function (form) {
        var _this = this;
        this.loaderStatus = this.loaderserviceService.showLoader();
        var postdata = {
            "user": this.model.user,
            "date": new Date(),
            "content": this.model.comment
        };
        this.httpdService.postComment(postdata, this.blogId).subscribe(function (data) {
            _this.changeState();
            _this.getBlogPostComments(_this.blogId);
            form.reset();
        }, function (catchError) {
            console.log('HTTP Error', catchError);
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        }, function () {
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        });
    };
    /**
     * This function change the url of icon image on like button
     * initially the url of the image is heart icon and once user clicks on it the uel will set to red heart image
     * it will appear as user has liked the blog
     */
    BlogdetailsComponent.prototype.likeBlogPost = function () {
        this.likeButtonImage = this.likeButtonImage === this.iconsBaseUrl + "heart.png" ? this.iconsBaseUrl + "heart-red.png" : this.iconsBaseUrl + "heart.png";
    };
    BlogdetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-blogdetails',
            templateUrl: './blogdetails.component.html',
            styleUrls: ['./blogdetails.component.scss'],
            animations: [
                Animations_1.Animations.hideshowanimation
            ]
        })
    ], BlogdetailsComponent);
    return BlogdetailsComponent;
}());
exports.BlogdetailsComponent = BlogdetailsComponent;
