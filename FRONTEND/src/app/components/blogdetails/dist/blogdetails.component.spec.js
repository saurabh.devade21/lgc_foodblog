"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MockDirective = void 0;
var firstcharonly_pipe_1 = require("./../../pipes/firstcharonly.pipe");
var forms_1 = require("@angular/forms");
var imageerror_directive_1 = require("./../../directives/imageerror.directive");
var testing_1 = require("@angular/core/testing");
var blogdetails_component_1 = require("./blogdetails.component");
var core_1 = require("@angular/core");
function MockDirective(options) {
    var metadata = {
        selector: options.selector,
        inputs: options.inputs,
        outputs: options.outputs
    };
    return core_1.Directive(metadata)(/** @class */ (function () {
        function MockDirectiveClass() {
        }
        return MockDirectiveClass;
    }()));
}
exports.MockDirective = MockDirective;
describe('BlogdetailsComponent', function () {
    var component;
    var fixture;
    var LoaderComponent = /** @class */ (function () {
        function LoaderComponent() {
        }
        LoaderComponent = __decorate([
            core_1.Component({ selector: 'app-loader', template: '' })
        ], LoaderComponent);
        return LoaderComponent;
    }());
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [blogdetails_component_1.BlogdetailsComponent, LoaderComponent, imageerror_directive_1.ImageerrorDirective, firstcharonly_pipe_1.FirstcharonlyPipe, MockDirective({
                    selector: '[appImageerror]',
                    inputs: [] // <--- empty, unless the directive has inputs
                })],
            imports: [forms_1.FormsModule]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(blogdetails_component_1.BlogdetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    // it('should create', () => {
    //   expect(component).toBeTruthy();
    // });
});
