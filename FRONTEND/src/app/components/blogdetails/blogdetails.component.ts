import { LoaderserviceService } from './../../services/loaderservice.service';
import { Serverendpoints } from './../../serverendpoints';
import { Animations } from './../../animations/Animations';
import { HttpdataService } from './../../services/httpdata.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { from } from 'rxjs';

/**
 *This component shows details and comments of the blog
 *it takes the blog id from url
 */
@Component({
  selector: 'app-blogdetails',
  templateUrl: './blogdetails.component.html',
  styleUrls: ['./blogdetails.component.scss'],
  animations: [
    Animations.hideshowanimation
  ]

})


export class BlogdetailsComponent implements OnInit {
  blogId: number;
  postDetails: any={};
  postComments: any=[];
  model: any = {};
  user: string;
  comment: string;
  iconsBaseUrl: string = this.serverendpoints.iconsBaseUrl;
  imagesBaseUrl: string = this.serverendpoints.imageBaseUrl;
  likeButtonImage: string = this.iconsBaseUrl + "heart.png";
  loaderStatus: boolean;

  constructor(
    private route: ActivatedRoute,
    private httpdService: HttpdataService,
    private router: Router,
    private serverendpoints: Serverendpoints,
    private loaderserviceService: LoaderserviceService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.blogId = +params.get('id');
      this.getBlogPostDetails(this.blogId);
    })
    //this.iconsBaseUrl=this.serverendpoints.iconsBaseUrl;
  }

/**
 * this function is repsonsible for getting the blog details of the blog id from the browser url
 * @param blogId id of the blog scraped from url
 * this.loaderserviceService.showLoader() & this.loaderserviceService.hideLoader() to hide and show the loader
 * then it calles getBlogPostDetailsById() method from the httpdservice and subscribe to that
 *
 * if th blog details are recieved succesully it will call the api for getting all the comments of that posts
 * it will store the recieved data in this.postdetails variable which will be shown on the page
 */
  getBlogPostDetails(blogId) {

    this.loaderStatus = this.loaderserviceService.showLoader();


    this.httpdService.getBlogPostDetailsById(blogId).subscribe(

      (data: any[]) => {
        this.postDetails = data;
        this.getBlogPostComments(blogId);

      },
      (catchError) => {
        console.log('HTTP Error', catchError);
        this.loaderStatus = this.loaderserviceService.hideLoader();
      },
      () => {
        this.loaderStatus = this.loaderserviceService.hideLoader();
      })


  }

  /**
   * the methods retrive all the comments posted for the blog
   * @param blogId id of the blog scraped from url
   * once the api is success it recieves the data and  using array.sort and arrow function sorts the array
   * by data and assign to this.postComments
   */
  getBlogPostComments(blogId) {
    this.loaderStatus = this.loaderserviceService.showLoader();


    this.httpdService.getBlogPostCommentsById(blogId).subscribe(

      (data: any[]) => {
        this.postComments = data.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());

      },
      (catchError) => {
        console.log('HTTP Error', catchError);
        this.loaderStatus = this.loaderserviceService.hideLoader();
      },
      () => {
        this.loaderStatus = this.loaderserviceService.hideLoader();
      })

  }

  /**
   * This functinality is implemented to assign the hide show animation on clicking the animation
   */
  currentState = 'hide';

  changeState() {
    this.currentState = this.currentState === 'hide' ? 'show' : 'hide';
  }

  /**
   *
   * @param form reference of form
   *
   * this method is used for posting the comment for a blog
   * this.loaderserviceService.showLoader() & this.loaderserviceService.hideLoader() to hide and show the loader
   * once the api is succesfully called it gets the all comments again from function getBlogPostComments()
   * and hides the form using changeState
   * finally it resets the form by clearing input fields
   *
   */
  postComment(form) {

    this.loaderStatus = this.loaderserviceService.showLoader();

    const postdata = {
      "user": this.model.user,
      "date": new Date(),
      "content": this.model.comment
    }

    this.httpdService.postComment(postdata, this.blogId).subscribe(

      (data: any[]) => {
        this.changeState();
        this.getBlogPostComments(this.blogId);
        form.reset();

      },
      (catchError) => {
        console.log('HTTP Error', catchError);
        this.loaderStatus = this.loaderserviceService.hideLoader();
      },
      () => {
        this.loaderStatus = this.loaderserviceService.hideLoader();
      })
  }

  /**
   * This function change the url of icon image on like button
   * initially the url of the image is heart icon and once user clicks on it the uel will set to red heart image
   * it will appear as user has liked the blog
   */
  likeBlogPost() {
    this.likeButtonImage = this.likeButtonImage === this.iconsBaseUrl + "heart.png" ? this.iconsBaseUrl + "heart-red.png" : this.iconsBaseUrl + "heart.png";
  }

}
