import { Serverendpoints } from './../../serverendpoints';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(private serverendpoints:Serverendpoints) { }
  iconsBaseUrl=this.serverendpoints.iconsBaseUrl;
  ngOnInit() {
  }

}
