import { Component, OnInit } from '@angular/core';
import { Animations } from 'src/app/animations/Animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [Animations.hideshowanimation,
  ]
})
export class HeaderComponent implements OnInit {
  currentState = false;
  constructor() { }

  ngOnInit() {
    if(window.innerWidth>=576){
      this.currentState = true;
    }
  }

/**
 *to hide and show the mobile menu based on the currentstate
 */

  changeState() {

    this.currentState = !this.currentState;

  }
}
