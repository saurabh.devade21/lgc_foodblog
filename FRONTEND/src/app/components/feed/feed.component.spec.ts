import { FilterpostPipe } from './../../pipes/filterpost.pipe';
import { AddblogpostComponent } from './../addblogpost/addblogpost.component';
import { LoaderComponent } from './../loader/loader.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedComponent } from './feed.component';
import { FormsModule } from '@angular/forms';

// describe('FeedComponent', () => {
//   let component: FeedComponent;
//   let fixture: ComponentFixture<FeedComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports:[FormsModule],
//       declarations: [ FeedComponent,LoaderComponent,AddblogpostComponent ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(FeedComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   // it('should create', () => {
//   //   expect(component).toBeTruthy();
//   // });

//   it('It should be equal to three',()=>{
//     expect(component.someFunction).toBe(3);
//   })
// });

