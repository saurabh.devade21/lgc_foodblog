import { LoaderserviceService } from './../../services/loaderservice.service';
import { Serverendpoints } from './../../serverendpoints';
import { HttpdataService } from './../../services/httpdata.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Animations } from 'src/app/animations/Animations';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
//import { AnimationEvent, transition, trigger, query, state, style, animate, animateChild, keyframes } from '@angular/animations';
/**
 *
 * this component is responsible for getting and displaying all the filed from the databse
 */
@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
  animations: [Animations.hideshowanimation,
  ]

})
export class FeedComponent implements OnInit {

  constructor(
    private httpdService: HttpdataService,
    private route: Router,
    private serverendpoints: Serverendpoints,
    private loaderserviceService: LoaderserviceService) { }
  public blogPosts = [];
  searchQuery: String;
  iconsBaseUrl = this.serverendpoints.iconsBaseUrl;
  floButtonIcon = this.iconsBaseUrl + "plus.png";
  loaderStatus: boolean;

  /**
   * As soon as the component is initiated this ngOnInit() method will
   * retrive the all posts
   */
  ngOnInit() {
    this.getAllBolgPost();
  }
/**
 *
 * @param blogId blogId of the post to be redirected
 * this methos will navigate the user to selected blog page details
 */
  ngNavigateToPost(blogId) {
    this.route.navigate(['blogdetails/', blogId]);
  }

  currentState = 'hide';

  /**
   * this method animates the add post detail from , hide and show
   * and also changes the icon of floating button from plus to cross
   */
  changeState() {
    this.currentState = this.currentState === 'hide' ? 'show' : 'hide';
    this.floButtonIcon = this.floButtonIcon === this.iconsBaseUrl + "plus.png" ? this.iconsBaseUrl + "close.png" : this.iconsBaseUrl + "plus.png";

  }

  /**
   * the methods retrive all the blog posts
   * once the api is success it recieves the data and  using array.sort and arrow function sorts the array
   * by data and assign to this.blogPosts
   */
  getAllBolgPost() {
    this.loaderStatus = this.loaderserviceService.showLoader();

    this.httpdService.getAllBlogPosts().subscribe(

      (data: any[]) => {
        this.blogPosts = data.sort((a, b) => new Date(b.publish_date).getTime() - new Date(a.publish_date).getTime());
      },
      (catchError) => {
        console.log('HTTP Error', catchError);
        this.loaderStatus = this.loaderserviceService.hideLoader();
      },
      () => {
        this.loaderStatus = this.loaderserviceService.hideLoader();
      })
  }

}
