"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FeedComponent = void 0;
var core_1 = require("@angular/core");
var Animations_1 = require("src/app/animations/Animations");
//import { AnimationEvent, transition, trigger, query, state, style, animate, animateChild, keyframes } from '@angular/animations';
/**
 *
 * this component is responsible for getting and displaying all the filed from the databse
 */
var FeedComponent = /** @class */ (function () {
    function FeedComponent(httpdService, route, serverendpoints, loaderserviceService) {
        this.httpdService = httpdService;
        this.route = route;
        this.serverendpoints = serverendpoints;
        this.loaderserviceService = loaderserviceService;
        this.blogPosts = [];
        this.iconsBaseUrl = this.serverendpoints.iconsBaseUrl;
        this.floButtonIcon = this.iconsBaseUrl + "plus.png";
        this.currentState = 'hide';
    }
    /**
     * As soon as the component is initiated this ngOnInit() method will
     * retrive the all posts
     */
    FeedComponent.prototype.ngOnInit = function () {
        this.getAllBolgPost();
    };
    /**
     *
     * @param blogId blogId of the post to be redirected
     * this methos will navigate the user to selected blog page details
     */
    FeedComponent.prototype.ngNavigateToPost = function (blogId) {
        this.route.navigate(['blogdetails/', blogId]);
    };
    /**
     * this method animates the add post detail from , hide and show
     * and also changes the icon of floating button from plus to cross
     */
    FeedComponent.prototype.changeState = function () {
        this.currentState = this.currentState === 'hide' ? 'show' : 'hide';
        this.floButtonIcon = this.floButtonIcon === this.iconsBaseUrl + "plus.png" ? this.iconsBaseUrl + "close.png" : this.iconsBaseUrl + "plus.png";
    };
    /**
     * the methods retrive all the blog posts
     * once the api is success it recieves the data and  using array.sort and arrow function sorts the array
     * by data and assign to this.blogPosts
     */
    FeedComponent.prototype.getAllBolgPost = function () {
        var _this = this;
        this.loaderStatus = this.loaderserviceService.showLoader();
        this.httpdService.getAllBlogPosts().subscribe(function (data) {
            _this.blogPosts = data.sort(function (a, b) { return new Date(b.publish_date).getTime() - new Date(a.publish_date).getTime(); });
        }, function (catchError) {
            console.log('HTTP Error', catchError);
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        }, function () {
            _this.loaderStatus = _this.loaderserviceService.hideLoader();
        });
    };
    FeedComponent = __decorate([
        core_1.Component({
            selector: 'app-feed',
            templateUrl: './feed.component.html',
            styleUrls: ['./feed.component.scss'],
            animations: [Animations_1.Animations.hideshowanimation,
            ]
        })
    ], FeedComponent);
    return FeedComponent;
}());
exports.FeedComponent = FeedComponent;
