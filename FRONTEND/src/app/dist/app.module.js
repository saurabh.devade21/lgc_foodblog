"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var animations_1 = require("@angular/platform-browser/animations");
var httpdata_service_1 = require("./services/httpdata.service");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var header_component_1 = require("./components/header/header.component");
var footer_component_1 = require("./components/footer/footer.component");
var feed_component_1 = require("./components/feed/feed.component");
var http_1 = require("@angular/common/http");
var blogdetails_component_1 = require("./components/blogdetails/blogdetails.component");
var firstcharonly_pipe_1 = require("./pipes/firstcharonly.pipe");
var forms_1 = require("@angular/forms");
var addblogpost_component_1 = require("./components/addblogpost/addblogpost.component");
var imageerror_directive_1 = require("./directives/imageerror.directive");
var filterpost_pipe_1 = require("./pipes/filterpost.pipe");
var scrolltotarget_directive_1 = require("./directives/scrolltotarget.directive");
var random_background_color_directive_1 = require("./directives/random-background-color.directive");
var tooltip_directive_1 = require("./directives/tooltip.directive");
var loader_component_1 = require("./components/loader/loader.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                header_component_1.HeaderComponent,
                footer_component_1.FooterComponent,
                feed_component_1.FeedComponent,
                blogdetails_component_1.BlogdetailsComponent,
                firstcharonly_pipe_1.FirstcharonlyPipe,
                addblogpost_component_1.AddblogpostComponent,
                imageerror_directive_1.ImageerrorDirective,
                filterpost_pipe_1.FilterpostPipe,
                scrolltotarget_directive_1.ScrolltotargetDirective,
                random_background_color_directive_1.RandomBackgroundColorDirective,
                tooltip_directive_1.TooltipDirective,
                loader_component_1.LoaderComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpClientModule,
                animations_1.BrowserAnimationsModule,
                forms_1.FormsModule
            ],
            providers: [httpdata_service_1.HttpdataService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
