import { Pipe, PipeTransform } from '@angular/core';

/**
 * this pipe is used for getting the first letter from the name of author
 * to display in the comment section with some random highlighted backgroud
 *
 */
@Pipe({
  name: 'firstcharonly'
})
export class FirstcharonlyPipe implements PipeTransform {

  transform(text: string): any {
    return text.slice(0,1);
  }

}
