"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FilterpostPipe = void 0;
var core_1 = require("@angular/core");
/**
 * this pipe is created for searching the data from array of object
 * used for search box in the feed component
 *
 */
var FilterpostPipe = /** @class */ (function () {
    function FilterpostPipe() {
    }
    /**
     *
     * @param items array of object from which data has to be searched
     * @param searchText data to be searched from array of object
     *
     * if items array is empty it will return null
     * if searchterm is empty it will return all the passed items
     * then it loweracses the search term for better results
     * using arrow function and array.includes method it returns the filtred array
     */
    FilterpostPipe.prototype.transform = function (items, searchText) {
        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }
        searchText = searchText.toLocaleLowerCase();
        return items.filter(function (post) {
            return post.title.toLocaleLowerCase().includes(searchText) || post.author.toLocaleLowerCase().includes(searchText)
                || post.description.toLocaleLowerCase().includes(searchText) || post.content.toLocaleLowerCase().includes(searchText);
        });
    };
    FilterpostPipe = __decorate([
        core_1.Pipe({
            name: 'filterpost'
        })
    ], FilterpostPipe);
    return FilterpostPipe;
}());
exports.FilterpostPipe = FilterpostPipe;
