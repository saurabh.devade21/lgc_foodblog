"use strict";
exports.__esModule = true;
var firstcharonly_pipe_1 = require("./firstcharonly.pipe");
var dummyData = [
    {
        "id": 1,
        "title": "Blog post #1",
        "author": "Melissa Manges",
        "publish_date": "2016-02-23",
        "slug": "blog-post-1",
        "description": "Utroque denique invenire et has.",
        "content": "<p>Utroque denique invenire et has. Cum case definitiones no, est dicit placerat verterem ne.</p> <p>In ius nonumy perfecto adipiscing, ad est cibo iisque aliquid, dicit civibus eum ei. Cum animal suscipit at, utamur utroque appareat sed ex.</p>",
        "poster": "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=700%2C636"
    }
];
describe('FirstcharonlyPipe', function () {
    it('create an instance', function () {
        var pipe = new firstcharonly_pipe_1.FirstcharonlyPipe();
        expect(pipe).toBeTruthy();
    });
    it('Should return S', function () {
        var pipe = new firstcharonly_pipe_1.FirstcharonlyPipe();
        expect(pipe.transform("SAURABH")).toBe('S');
    });
    it('Should return Empty String', function () {
        var pipe = new firstcharonly_pipe_1.FirstcharonlyPipe();
        expect(pipe.transform("")).toBe('');
    });
});
