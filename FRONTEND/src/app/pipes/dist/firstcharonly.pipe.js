"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FirstcharonlyPipe = void 0;
var core_1 = require("@angular/core");
/**
 * this pipe is used for getting the first letter from the name of author
 * to display in the comment section with some random highlighted backgroud
 *
 */
var FirstcharonlyPipe = /** @class */ (function () {
    function FirstcharonlyPipe() {
    }
    FirstcharonlyPipe.prototype.transform = function (text) {
        return text.slice(0, 1);
    };
    FirstcharonlyPipe = __decorate([
        core_1.Pipe({
            name: 'firstcharonly'
        })
    ], FirstcharonlyPipe);
    return FirstcharonlyPipe;
}());
exports.FirstcharonlyPipe = FirstcharonlyPipe;
