import { Pipe, PipeTransform } from '@angular/core';

/**
 * this pipe is created for searching the data from array of object
 * used for search box in the feed component
 *
 */
@Pipe({
  name: 'filterpost'
})
export class FilterpostPipe implements PipeTransform {

  /**
   *
   * @param items array of object from which data has to be searched
   * @param searchText data to be searched from array of object
   *
   * if items array is empty it will return null
   * if searchterm is empty it will return all the passed items
   * then it loweracses the search term for better results
   * using arrow function and array.includes method it returns the filtred array
   */
  transform(items: any[], searchText: String): any {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();


    return items.filter(post => {
      return post.title.toLocaleLowerCase().includes(searchText) || post.author.toLocaleLowerCase().includes(searchText)
       || post.description.toLocaleLowerCase().includes(searchText)|| post.content.toLocaleLowerCase().includes(searchText)
    });

  }

}
