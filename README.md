# Introduction
This was a test project for dveloper challenge for LetsGetChecked. The challenge was to create a BlogApplication using Angular Framework. All the resources and requirement can be found in Github [repo](https://github.com/LetsGetChecked/developer-challenge-api).


# Demo
Visit [http://www.saurabh.rocks](http://www.saurabh.rocks) for live demo

# Requirements
To test this application following needs to be installed on your system
 - Modern web browser (Google Chrome Recommended)
 - [npm](https://www.npmjs.com/)
 - [node.js](https://nodejs.org/en/)
 - [Angular CLI](https://cli.angular.io/)
  
# Instructions
- Clone the repository from the **[gitlab](https://gitlab.com/saurabh.devade21/lgc_foodblog)** or use the zip folder provided in mail
 - Unzip the file **saurabh-frontend-test**.zip
 - Go to BACKEND folder and run 

> npm install
> nmp start

it will start the node.js app on port 3000

- Then go to FRONTEND and run

> npm install
> ng serve

This will start Angular app with the url http://localhost:4200/




 
# Implementation

To achieve the above functinalities mentioned in the GithubRepo mentioned abobe following features of **Angular 8** are used
**Components** - 
1. AddblogpostComponent - To add the New Blog posts (To demonstrate component hierarchy)
2. BlogdetailsComponent - To show the details of individual blog on seperate page
3. FeedComponent - To Display all the post on homepage
4. FooterComponent - For footer
5. HeaderComponent - For displaying the header on all the pages
6. LoaderComponent - To show the loading screen across the entire app

**Services  -** 
1. HttpdataService - All the api management is done through this service
2. LoaderserviceService - To make loader component available across the application

**Directives -** 
1. ImageerrorDirective - To show the image-not-available image on all the broken images
2. RandomBackgroundColorDirective - To apply the random background colors on selected element
3. ScrolltotargetDirective - To scroll to particular element with smooth effect
4. TooltipDirective - To copy the text to clipboard and showing the tooltip with text copied

**Pipes**
1. FilterpostPipe - To filter the post shown on home page based on search text
2. FirstcharonlyPipe - To return the first letter from the string provided

**Animation**
1. To bring the hide and show effect on the elements

The technical details of each has been provided in the comments section of each component,service,pipes,directives,animations etc.
While devloping the app **semantic markup, SEO and accessibility** has been considred throughout
CSS preprocessor **SCSS** is used.
# Extra Feature
1. Site Deployed to Live server - [http://www.saurabh.rocks](http://www.saurabh.rocks) domain taken from **name.com free domain name**
2. Node.js on heroku cloud
3. Angular app on Google CLoud Platform
4. Hubsot ChatBot added for live interaction with developer
5. elfsight social media share added
6. User can add the blogpost with image

# FAQs
1. On what browsers this page is tested?
**Ans** : This webpage is tested on Google Chrome, Mozilla Firefox, Microsoft Edge, Duck Duck Go & Brave
2. On what Resolutions this pages is tested?
**Ans**: This webpage is tested for Desktop, Tablet & Mobile resolution.
3. What are the technolgies and libraries are used in this project?
**Ans**: Angular 8, Typescript, SCSS, CSS, Node.js.

# Known Issues
 - This webpage dosent perform as expected on internet explorer browser
  
# Future Scope
 - Performance of webpage can be tested on more wide range of browsers.
# Conclusion
This was a great learing experience from the devlopers prospective. Got chance to go deeper into some of the angular functinalities.